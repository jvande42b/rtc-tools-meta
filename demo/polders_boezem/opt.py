from pathlib import Path

import casadi as ca

from rtc_tools_meta.meta_optimization_problem import MetaOptimizationProblem
from rtc_tools_meta.util import collect_model_instance


base_path = Path(__file__).parent.resolve()

polders = collect_model_instance("LinkedPolders", base_path / "linked_polders/opt.py")
boezem = collect_model_instance("Boezem", base_path / "boezem/src/opt.py")


class PoldersBoezem(MetaOptimizationProblem):
    submodels = {"LinkedPolders": polders, "Boezem": boezem}
    variable_mappings = [
        ("LinkedPolders::polder10::Outflow_Q", "Boezem::Polder_Inflow_Q", "-")
    ]

    def meta_objective(self):
        return -ca.sum1(self.states_in("Boezem::Polder_Inflow_Q"))


if __name__ == "__main__":
    problem = PoldersBoezem()
    problem.optimize()
