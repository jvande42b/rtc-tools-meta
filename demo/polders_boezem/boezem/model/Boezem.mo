model Boezem
  // Model Elements
  Deltares.ChannelFlow.SimpleRouting.BoundaryConditions.Inflow Inflow;
  Deltares.ChannelFlow.SimpleRouting.BoundaryConditions.Inflow Polder_Inflow;
  Deltares.ChannelFlow.SimpleRouting.BoundaryConditions.Inflow Outflow;
  Deltares.ChannelFlow.SimpleRouting.Nodes.Node Node(nin=2, nout=1);
  Deltares.ChannelFlow.SimpleRouting.Branches.Integrator Channel(V(min = 400.0, max=600.0));
  // Inputs
  input Real Inflow_Q(fixed = true) = Inflow.Q;
  input Real Polder_Inflow_Q(fixed = false, min=-5.0, max=5.0) = Polder_Inflow.Q;
  input Real Outflow_Q(fixed = false, min=0.0, max=0.45) = Outflow.Q;
  // Outputs
  output Real Channel_V = Channel.V;
equation
  connect(Polder_Inflow.QOut, Node.QIn[1]);
  connect(Inflow.QOut, Node.QIn[2]);
  connect(Node.QOut[1], Channel.QIn);
  connect(Channel.QOut, Outflow.QOut);
end Boezem;
