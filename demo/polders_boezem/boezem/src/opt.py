from rtc_tools_meta.meta_mixin import MetaMixin

from rtctools.optimization.collocated_integrated_optimization_problem import (
    CollocatedIntegratedOptimizationProblem,
)
from rtctools.optimization.csv_mixin import CSVMixin
from rtctools.optimization.goal_programming_mixin import GoalProgrammingMixin, StateGoal
from rtctools.optimization.modelica_mixin import ModelicaMixin
from rtctools.util import run_optimization_problem


class StateGoalPlus(StateGoal):
    """StateGoal, but with initialization parameters"""

    def __init__(self, opt_prob, state, priority, ts_min=None, ts_max=None, order=2):
        # Set goal parameters
        self.state = state
        self.priority = priority
        if ts_max is not None:
            self.target_min = opt_prob.get_timeseries(ts_min)
        if ts_max is not None:
            self.target_max = opt_prob.get_timeseries(ts_max)
        self.order = order

        # Set output timeseries ids
        self.violation_timeseries_id = "violation_{}_{}_{}".format(
            state, ts_min, ts_max
        )
        self.function_value_timeseries_id = state

        # Do rest of StateGoal initialization
        super().__init__(opt_prob)


class Boezem(
    MetaMixin,
    GoalProgrammingMixin,
    CSVMixin,
    ModelicaMixin,
    CollocatedIntegratedOptimizationProblem,
):
    goal_priority_dict = {"water_level_goal": 1}

    def path_goals(self):
        g = super().path_goals()

        # Add RangeGoal on water volume states with a priority of 1
        g.append(
            StateGoalPlus(
                self,
                state="Channel.V",
                priority=self.goal_priority_dict["water_level_goal"],
                ts_min="Channel.V_min",
                ts_max="Channel.V_max",
            )
        )
        return g


if __name__ == "__main__":
    run_optimization_problem(Boezem)
