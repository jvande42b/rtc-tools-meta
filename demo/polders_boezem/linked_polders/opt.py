from pathlib import Path

import casadi as ca

from rtc_tools_meta.meta_optimization_problem import MetaOptimizationProblem
from rtc_tools_meta.util import collect_model_instance

base_path = Path(__file__).parent.resolve()
all_polders = {
    f"polder{i}": collect_model_instance("Polder", base_path / f"polder{i}/src/opt.py")
    for i in range(1, 11)
}


class LinkedPolders(MetaOptimizationProblem):
    submodels = all_polders
    variable_mappings = [
        (f"polder{i}::Outflow_Q", f"polder{i+1}::Inflow_Q", "-") for i in range(1, 10)
    ]

    def meta_objective(self):
        return ca.sumsqr(
            ca.vertcat(
                *[
                    self.states_in(f"{submodel}::incurred_damage")
                    for submodel in self.submodels.keys()
                ]
            )
        )


if __name__ == "__main__":
    problem = LinkedPolders()
    problem.optimize()
