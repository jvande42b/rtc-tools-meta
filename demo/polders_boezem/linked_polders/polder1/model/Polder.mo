model Polder
  // Model Elements
  Deltares.ChannelFlow.SimpleRouting.BoundaryConditions.Inflow Inflow;
  Deltares.ChannelFlow.SimpleRouting.BoundaryConditions.Inflow Outflow;
  Deltares.ChannelFlow.SimpleRouting.Storage.Storage Storage(V(min = 8000.0, max=42000.0));
  // Inputs
  input Real Inflow_Q(fixed = true) = Inflow.Q;
  input Real Outflow_Q(fixed = false, min=-5.0, max=5.0) = Outflow.Q;
  // Outputs
  output Real Storage_V = Storage.V;
  output Real incurred_damage = (30000.0 - Storage.V) / 12000.0;
equation
  connect(Inflow.QOut, Storage.QIn);
  connect(Storage.QOut, Outflow.QOut);
end Polder;
