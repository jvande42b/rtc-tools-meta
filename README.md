Installation
------------

Requires python3 (& pip) and git

From command line (Powershell)

1. Clone this repo: `git@gitlab.com:jvande42b/rtc-tools-meta.git`
2. Navigate to repo dir: `cd rtc-tools-meta`
3. Make venv: `python -m venv venv`
4. Update pip: `pip install --upgrade pip`
5. Activate: `venv\Scripts\Activate.psl`
6. Install package in venv: `pip install .`
7. Run: `python opt.py` in one of the demo dirs.