API Reference
=============

This page is auto-generated from the source code and is intended to be used as
an API reference.  See :doc:`quickstart` to get started the easy way.

MetaOptimizationProblem
-----------------------

.. autoclass:: rtc_tools_meta.meta_optimization_problem.MetaOptimizationProblem
   :members: submodels, variable_mappings, pop_submodel, pre, post, transcribe, optimize, interface_variables, meta_objective
   :show-inheritance:

MetaMixin
---------

.. autoclass:: rtc_tools_meta.meta_mixin.MetaMixin
   :show-inheritance:

collect_model_instance()
------------------------

.. autofunction:: rtc_tools_meta.util.collect_model_instance
