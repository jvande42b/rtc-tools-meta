Examples
========

Here is a quick description of the examples found in the demo directory. To
understand the variables and classes in the code blocks, check out :doc:`quickstart`.


Linked Polders
---------------

This example demonstrates the aggregation of 10 identical polders connected in
series:

.. image:: ../images/linked_polders.*

Water flows in at 0.5 CMS into one end of the polders. The polders can
only pump into their immediate neighbors. At the other end from the inflow,
there is a polder that can pump water out of the network, but it can only do so
at a rate of 0.45 CMS. All the polders are at the ideal max water volume, so
none want to store the water. The polders must decide how to allocate the
inflow. There are 10 of these polders. Their Modelica models look like this:

.. literalinclude:: ../../demo/linked_polders/polder2/model/Polder.mo
  :language: modelica

And their python class looks like this:

.. literalinclude:: ../../demo/linked_polders/polder1/src/opt.py
  :language: python
  :pyobject: Polder

.. _linked_polders_example:

Here is the aggregator class in a separate file:

.. literalinclude:: ../../demo/linked_polders/opt.py
  :language: python

Being egalitarian people, the operators decide that the high water levels must
be shared as much as possible between the models. They give each model a damage
function correlating the cost of high water with the water level, and penalize
this expression with a second-order penalty. This means that when all the
objectives from each polder are added together, IPOPT will try to reduce the
largest damage function first. The net effect is that when IPOPT is done, the
models will be sharing the inflows (and the damage) as evenly as possible, and
as little as possible.

In the results plotted below, it is immediately apparent that the polders are
taking equal damage. They do this by each storing an equal share of the inflow,
while passing the rest on.

.. plot:: content/example_result_plots/linked_polders.py


Polders and Boezem
------------------

In this example we add a meta model on top of the previous example. The last
polder drains into a boezem with no tolerance for flooding:

.. image:: ../images/polders_boezem.*

To make sure the flow into the boezem is sufficiently small, we aggregate the
boezem model into the system. Here is the boezem as a Modelica model:

.. literalinclude:: ../../demo/polders_boezem/boezem/model/Boezem.mo
  :language: modelica

And its python class looks like this:

.. literalinclude:: ../../demo/polders_boezem/boezem/src/opt.py
  :language: python
  :pyobject: Boezem

The damage function tells IPOPT to find a solution that maximizes the amount of
water coming from the ``LinkedPolders`` model, subject to the hard constraints
volume constraints in the model.

.. _polders_boezem_example:

Here is the meta model:

.. literalinclude:: ../../demo/polders_boezem/opt.py
  :language: python


Note that to run, we simply call the `optimize()` method!


In the results plotted below, it is immediately apparent that the polders are
taking equal damage. They do this by each storing an equal share of the inflow,
while passing the rest on. Furthermore, The boezem does not take its share of
damage- it only takes the flow that it can safely handle.

.. plot:: content/example_result_plots/polders_boezem.py
