.. rtc-tools-meta documentation master file, created by
   sphinx-quickstart on Fri Oct 12 22:25:22 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.


.. |deltares_logo| image:: images/deltares_logo.*
   :target: https://www.deltares.nl/
   :align: middle

.. |kisters_logo| image:: images/KISTERS_logo_2014.*
   :target: https://water.kisters.de/en/
   :align: middle

*This library is a collaborative effort between*:

+----------------+-----------------+
| |kisters_logo| | |deltares_logo| |
+----------------+-----------------+

rtc-tools-meta
==============

This is the documentation for ``rtc-tools-meta`` (Meta), a distributed model
predictive control (DMPC) library. Meta finds the interface conditions between
independent `rtc-tools <https://rtc-tools.readthedocs.io/en/latest/>`_ models.
This allows users of rtc-tools to model complex systems as a network of modular
submodels that can run independently, and use Meta determine how the models
interact.

Overview
--------

Meta allows you to easily aggregate existing ``rtc-tools`` models into larger
ones, and then do an optimization to find good boundary conditions between those
models. Once the boundary conditions are optimized and the results are imposed
on the submodels, Meta runs the submodels as usual. Here is a visual overview of
the process:

.. image:: images/overview.*

Strengths and Limitations of ``rtc-tools-meta``
-----------------------------------------------

Before you get started, here are some important strengths and limitations of this
library to be aware of.

Strengths
+++++++++

#. By aggregating the model, interface variables are *by definition* equal. In
   contrast, an iterative scheme (where submodels take turns running) has no
   guarantee of finding a solution with consistent interface variables between
   the submodels, and less still of finding a reasonably optimal one. An
   aggregated model can take consistent interface variables for granted, and
   focus on finding their best possible values.
#. The Meta api is *recursive-ready*. This means you can aggregate meta-models
   into meta-meta-models, etc. And you can have unique meta-objectives for each
   meta level. As long as the largest model is small enough to run, you can
   connect as large a network as you like.

Limitations
+++++++++++

#. There is not yet any model simplification. This means that the
   meta problem is the same size as the sum of the subproblems. If your models
   are already quite large, you *may* find you can't use this library.
#. The Meta optimization problem has a single meta-objective function.
   This means that the models must be linear, and the meta-objective *must* be
   at least convex, or convergence will be an issue. Keep in mind that
   interior-point methods like IPOPT can optimize massive problems if they are
   convex, and Linear Programming algorithms can handle even larger problems.
   This is a good thing with regard to Limitation #1. (Note that integration
   with HomotopyMixin is planned- meta models would run with the linear
   equations and submodels would resolve the non-linear hydraulics)
#. Variables that interface between two models *must* be collocated at identical
   timesteps. In practice, this means that all models are run at the same
   timestep for the same duration, unless you get funky with the ``times()``
   method in ``rtc-tools``. 


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   content/quickstart
   content/autodoc_api
   content/examples
